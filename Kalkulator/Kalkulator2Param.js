function kalkulator2Input(input1, operator, input2){
    const phi = 3.14;
    if (operator === 1) return input1 + input2;
    else if (operator === 2) return input1 - input2;
    else if (operator === 3) return input1 / input2;
    else if (operator === 4) return input1 * input2;
    else if (operator === 8) return ((phi * input1 ** 2) * input2);
}

module.exports =  kalkulator2Input;