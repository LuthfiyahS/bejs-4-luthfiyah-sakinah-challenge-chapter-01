
const kalkulator1Input = require("./Kalkulator/Kalkulator1Param.js");
const kalkulator2Input = require("./Kalkulator/Kalkulator2Param.js");

const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const validOperators = ["1", "2", "3", "4", "5", "6", "7", "8"];

rl.question("Selamat Datang di Aplikasi ini, siapa nama anda? ", (answer) => {
    console.log(`\nHallo ${answer}, Silahkan pilih apa yang ingin dijalankan?`);
    console.log("=====================================================================================");
    console.log("1. Tambah(+)          2. Kurang(-)               3. Bagi(/)                4. Kali(*)");
    console.log("5. Akar Kuadrat(^2)   6. Luas Persegi(L Persegi) 7. Volume Kubus(V Kubus)  8. Volume Tabung(V Tabung)\n");
    operasi();
})

function operasi() {
    rl.question("Silahkan Masukkan nomor Pilihan Menu Anda? ", (answer) => {
        let ans = parseInt(answer);
        if (!validOperators.includes(answer)) {
            console.log("Inputan yang ada berikan tidak sesuai ");
            return rl.close();
        }
        let kata1, kata2;
        if (ans >= 1 && ans <= 4) {
            kata1 = "Bilangan pertama: ";
            kata2 = "Bilangan kedua: ";
        } else if (ans == 5) {
            kata1 = "Bilangan untuk dikuadratkan: ";
        } else if (ans == 6 || ans == 7) {
            kata1 = "Sisi : ";
        } else {
            kata1 = "Panjang Jari Jari(r) : ";
            kata2 = "Tinggi Kubus : ";
        }

        rl.question(`Masukkan ${kata1}`, (Input1) => {
            const input1 = parseInt(Input1);
            if (isNaN(input1)) {
                console.log("Inputan yang ada berikan bukan angka");
                return rl.close();
            }
            if (ans != 5 && ans != 6 && ans != 7) {
                rl.question(`Masukkan ${kata2}`, (Input2) => {
                    const input2 = parseInt(Input2);
                    if (isNaN(input2)) {
                        console.log("Inputan yang ada berikan bukan angka");
                        return rl.close();
                    }
                    console.log(`Memuat Hasil...`)
                    
                    setTimeout(() => console.log(`Hasil Kalkulasi: ${kalkulator2Input(input1, ans, input2)}`), 1000)
                    setTimeout(() => tanya(), 3000);
                });
            }
            if (ans >= 5 && ans <= 7) {
                console.log(`Memuat Hasil...`)
                    setTimeout(() => console.log(`Hasil Kalkulasi:  ${kalkulator1Input.kalkulator1Input(input1, ans)}`), 1000)
                setTimeout(() => tanya(), 3000);
            }
        });
    })
}

function tanya() {
    rl.question("\nApakah ingin melakukan kalkulasi lagi? (y/n)", (answer) => {
        if (answer == "n") {
            console.log("Sampai Jumpa!");
            rl.close();
        } else if (answer == "y") {
            operasi();
        } else {
            tanya();
        }
    })
}